###Description

This module provides a visual way to easily input tags within textfields on 
webforms. This might be useful in situations where a field value needs to 
be a comma separated list. Duplicate tags are flagged visually, and mobile 
devices are also supported.

###Requirements
- Webform 7.x-4.x
- Libraries 7.x-2.0

###Installation 
Download the latest release of Selectize.js here:
http://selectize.github.io/selectize.js/

Extract into your libraries folder and rename folder, 
e.g. /sites/all/libraries/selectize.js

When editing a textfield component, check the Tag input field, save 
component and reload your webform.

###Browser support
Works in modern browsers and IE9+
