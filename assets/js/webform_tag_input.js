/**
 * @file
 */
(function ($) {
  'use strict';
  $(document).ready(function () {
    $('.field-tags-input').selectize({
      delimiter: ',',
      persist: false,
      create: function (input) {
        return {
          value: input,
          text: input
        };
      }
    });
  });
})(jQuery);
