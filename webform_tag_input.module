<?php

/**
 * @file
 * Webform Tag Input module.
 */

/**
 * Implements hook_libraries_info().
 */
function webform_tag_input_libraries_info() {
  $libraries['selectize.js'] = array(
    'name' => 'selectize.js',
    'vendor url' => 'http://selectize.github.io/selectize.js/',
    'download url' => 'http://selectize.github.io/selectize.js/',
    'version callback' => 'webform_tag_input_version_callback',
    'files' => array(
      'js' => array('dist/js/standalone/selectize.min.js'),
      'css' => array('dist/css/selectize.css'),
    ),
  );

  return $libraries;
}

/**
 * Callback for version while fix for package.json is in Libraries dev.
 */
function webform_tag_input_version_callback() {
  return TRUE;
}

/**
 * Implements hook_webform_component_defaults_alter().
 */
function webform_tag_input_webform_component_defaults_alter(&$defaults, $type) {
  switch ($type) {
    case 'textfield':
      $defaults['extra']['tag_input'] = 0;
      break;
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function webform_tag_input_form_webform_component_edit_form_alter(&$form, &$form_state) {
  $component = $form_state['build_info']['args'][1];
  if (in_array($component['type'], array('textfield'))) {
    $form['extra']['tag_input'] = array(
      '#type' => 'checkbox',
      '#default_value' => isset($component['extra']['tag_input']) ? $component['extra']['tag_input'] : '',
      '#title' => 'Tag input',
      '#description' => t('This turns a textfield into a tag input. Used where ideal output is a comma separated list.'),
    );
  }
}

/**
 * Implements hook_webform_component_render_alter().
 */
function webform_tag_input_webform_component_render_alter(&$element, &$component) {
  if (isset($component['extra']['tag_input']) && $component['extra']['tag_input']) {
    $element['#attributes']['class'] = array('field-tags-input');
    $element['#attached']['libraries_load'][] = array('selectize.js');
    $element['#attached']['js'][] = drupal_get_path('module', 'webform_tag_input') . '/assets/js/webform_tag_input.js';
  }
}
